# carousel.whd-design.co.uk  #

Simon Hewitt
+44 7500 883122
simon.d.hewitt@gmail.com


##Demo of a Guided Access with iPhone

See `https://support.apple.com/en-gb/HT202612`
for the Apple instructions on how to set up a guided Access App.

The App used is just a very simple Swift 3 App that contains a WKWebView that calls the S3 Web Page, :-   

The web site used for this demo is `http:\\carousel.whd-design.co.uk`
which is a static web site using S3. My AWS Account - `simonAdmin`, 
and S3 bucket, needs to be the same name as the Web page, 
so bucket is also called `carousel.whd-design.co.uk`.

Its a simple index.html file that cycles six images in the folder images/,
using bootstrap carousel, see _lost it_, it was  a W3 Schools page. At present all the code is in the `index.html`, it includes Bootstrap etc, but otherwise is pretty simple. 

TODO: I may add a few links to make it look better.

The iOS/Swift and HTML (called PHP but it isn't) is in BitBucket at `https://bitbucket.org/simon_hewitt/dpt-demo`  

20/7/17 - fix column overlap, just made column 1 sm-5, column 2 sm-3, col3 unchanged at sm-4


